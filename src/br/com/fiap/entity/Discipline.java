package br.com.fiap.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "discipline")
public class Discipline {
	
	@Id
	@Column(name = "disciplineId")
	private int disciplineId;
	
	@Column(name = "name", length = 100)
	private String name;
	
	@ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "courseId") 
	private Course course;
	
	@ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "professorId") 
	private Professor professor;

	public int getDisciplineId() {
		return disciplineId;
	}
	public void setDisciplineId(int disciplineId) {
		this.disciplineId = disciplineId;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	
	public Professor getProfessor() {
		return professor;
	}
	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
	
	@Override
	public String toString() {
		return name;
	}
		
	
}