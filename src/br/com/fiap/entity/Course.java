package br.com.fiap.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "course")
//@NamedQueries({
//	@NamedQuery(name = Course.QUERY_LIST_ALL_COURSES, query = "SELECT c FROM course c"),
//	@NamedQuery(name = Course.QUERY_LIST_COURSE_BY_ID, query = "SELECT c FROM course c WHERE c.id = :id")
//}) 
public class Course {
	
//	public static final String QUERY_LIST_ALL_COURSES = "QUERY_LIST_ALL_COURSES";
//	public static final String QUERY_LIST_COURSE_BY_ID = "QUERY_LIST_COURSE_BY_ID";
	
	
	@Id
	@Column(name = "courseId")
	private int courseId;
	
	@Column(name = "name", length = 100)
	private String name;
	
	@OneToMany
	(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "course")
	private List<Student> students = new ArrayList<Student>();
	
	@OneToMany
	(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "course")
	private List<Discipline> disciplines = new ArrayList<Discipline>();

		
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Student> getStudents() {
		return students;
	}
	public void setStudents(List<Student> students) {
		this.students = students;
	}
	
	public List<Discipline> getDisciplines() {
		return disciplines;
	}
	public void setDisciplines(List<Discipline> disciplines) {
		this.disciplines = disciplines;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	
			
}