package br.com.fiap.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "professor")
//@NamedQueries({
//	@NamedQuery(name = Professor.QUERY_LIST_ALL_PROFESSORS, query = "SELECT p FROM professor p"),
//	@NamedQuery(name = Professor.QUERY_LIST_PROFESSOR_BY_ID, query = "SELECT p FROM professor p WHERE p.id = :id")
//}) 

public class Professor {
	
	
//	public static final String QUERY_LIST_ALL_PROFESSORS = "QUERY_LIST_ALL_PROFESSORS";
//	public static final String QUERY_LIST_PROFESSOR_BY_ID = "QUERY_LIST_PROFESSOR_BY_ID";
	
	
	@Id
	@Column(name = "professorId")
	private int professorId;
	
	@Column(name = "name", length = 100)
	private String name;
	
	@OneToMany
	(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "professor")
	private List<Discipline> disciplines = new ArrayList<Discipline>();
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="roleId")
	private Role role;

		
	public int getProfessorId() {
		return professorId;
	}
	public void setProfessorId(int professorId) {
		this.professorId = professorId;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Discipline> getDisciplines() {
		return disciplines;
	}
	public void setDisciplines(List<Discipline> disciplines) {
		this.disciplines = disciplines;
	}
	
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	
	@Override
	public String toString() {
		return name;
	}

	
}