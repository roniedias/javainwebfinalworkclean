package br.com.fiap.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user")
//@NamedQueries({
//	@NamedQuery(name = User.QUERY_LIST_USER_BY_EMAIL, query = "SELECT p FROM user p WHERE p.email = :email")
//}) 
public class User {
	
//	public static final String QUERY_LIST_USER_BY_EMAIL = "QUERY_LIST_USER_BY_EMAIL";
	
	
	@Id
	@Column(name = "userId")
	private int userId;
	
	@Column(name = "email", length = 100)
	private String email;
	
	@Column(name = "password", length = 100)
	private String password;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="roleId")
	private Role role;
	
	@Column(name = "studentprofessorId")
	private int studentProfessorId;
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public int getStudentProfessorId() {
		return studentProfessorId;
	}

	public void setStudentProfessorId(int studentProfessorId) {
		this.studentProfessorId = studentProfessorId;
	}

	@Override
	public String toString() {
		return email;
	}

	
}