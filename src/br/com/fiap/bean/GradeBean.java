package br.com.fiap.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import br.com.fiap.dao.GenericDao;
import br.com.fiap.entity.Discipline;
import br.com.fiap.entity.Grade;
import br.com.fiap.entity.Student;

@ManagedBean(name = "gradeBean" )
@RequestScoped
public class GradeBean implements Serializable {
	
	private static final long serialVersionUID = -7755771087129569019L;

	private Grade grade;
	
	public GradeBean(){
		grade = new Grade();
	}
	
	public Grade getGrade() {
		return grade;
	}
	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	public void setGradeStudentId(int studentId) {
		System.out.println(studentId);
		grade.getStudent().setId(studentId);
	}
	
	public void setGradeDisciplineId(int disciplineId) {
		grade.getDiscipline().setDisciplineId(disciplineId);
	}

		
	public String addGrade(){
		
		Map<String, String> parameterMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String studentId = parameterMap.get("studentId");
		String disciplineId = parameterMap.get("disciplineId");
				
		Student s = new Student();
		s.setId(Integer.valueOf(studentId));
		grade.setStudent(s);
		
		Discipline d = new Discipline();
		d.setDisciplineId(Integer.valueOf(disciplineId));
		grade.setDiscipline(d);
		
		if(((grade.getGrade1() + grade.getGrade2() + grade.getGrade3()) / 3) >= 7.0) {
			grade.setStatus("Aprovado");
		}
		else {
			grade.setStatus("Reprovado");
		}
		
		GenericDao<Grade> dao = new GenericDao<Grade>(Grade.class);
		dao.add(grade);

		return "/success";
	}
	
	public List<Grade> listGrades(){
		GenericDao<Grade> dao = new GenericDao<Grade>(Grade.class);
		return dao.list();
	}	
	
	public List<Grade> gradeByStudentId(int studentId) {
		GenericDao<Grade> dao = new GenericDao<Grade>(Grade.class);
		List<Grade> grades = dao.list();
		List<Grade> gradesRet = new ArrayList<Grade>();
		for(Grade g : grades) {
			if(g.getStudent().getStudentId() == studentId) {
				gradesRet.add(g);
			}
		}
		return gradesRet;
	}
	

}

