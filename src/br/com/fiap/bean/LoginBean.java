package br.com.fiap.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.com.fiap.dao.GenericDao;
import br.com.fiap.dao.UserDao;
import br.com.fiap.entity.Professor;
import br.com.fiap.entity.User;

@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

	private static final long serialVersionUID = -2559732822190380389L;

	private User user;

	private List<String> studentNameByProfessorList;

	public LoginBean() {
		user = new User();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String login() {
		UserDao userDao = new UserDao(User.class);
		user = userDao.login(user);
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);

		if (user == null) {
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario / Senha Inválidos", "Usuario / Senha Inválidos");
            FacesContext.getCurrentInstance().addMessage(null, fm);
            user = new User();
            return "/login.xhtml";
			
		} else if (user != null && user.getRole().getRoleId() == 1) {

			return "/restrict/admin/index.xhtml?faces-redirect=true";
		} else if (user != null && user.getRole().getRoleId() == 2) {

			return "/restrict/aluno/student-list.xhtml?faces-redirect=true";
		} else if (user != null && user.getRole().getRoleId() == 3) {

			return "/restrict/professor/list/course-list.xhtml?faces-redirect=true";
		}

		return "/login.xhtml";
	}

	public List<String> loadStudentNameByProfessorList() {
		// String query = "SELECT g.studentName FROM grade g, discipline d, user
		// u where g.disciplineId = d.disciplineId and d.professorId =
		// u.studentProfessorId and userId = '" + userId + "'";
		// GenericDao<String> dao = new GenericDao<String>(String.class);
		// return dao.nativeQuery(query);
		GenericDao<Professor> dao = new GenericDao<Professor>(Professor.class);
		Professor professor = dao.find(user.getStudentProfessorId());
		List<String> studentNamesByProfessorName = new ProfessorBean().studentNamesByProfessorName(professor.getName());
		return studentNamesByProfessorName;
	}

	public List<String> getStudentNameByProfessorList() {
		return studentNameByProfessorList;
	}

	public void setStudentNameByProfessorList(List<String> studentNameByProfessorList) {
		this.studentNameByProfessorList = studentNameByProfessorList;
	}

	public boolean isMyStudent(String studentName) {
		return studentNameByProfessorList.contains(studentName);
	}

}
