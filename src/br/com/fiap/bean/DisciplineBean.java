package br.com.fiap.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.com.fiap.dao.GenericDao;
import br.com.fiap.entity.Course;
import br.com.fiap.entity.Discipline;
import br.com.fiap.entity.Professor;
import br.com.fiap.entity.Student;

@ManagedBean(name = "disciplineBean" )
@RequestScoped
public class DisciplineBean implements Serializable {
	
	private static final long serialVersionUID = 5448994962555479528L;

	private Discipline discipline;
	
	public DisciplineBean(){
		discipline = new Discipline();
	}
	
	public Discipline getDiscipline() {
		return discipline;
	}
	public void setDiscipline(Discipline discipline) {
		this.discipline = discipline;
	}

	public String addDiscipline(String courseName, String professorName){
		try {
			GenericDao<Discipline> dao = new GenericDao<Discipline>(Discipline.class);
			GenericDao<Course> courseDao = new GenericDao<Course>(Course.class);
			GenericDao<Professor> professorDao = new GenericDao<Professor>(Professor.class);
			
			Course course = courseDao.findByName(courseName);
			discipline.setCourse(course);
			
			Professor professor = professorDao.findByName(professorName);
			discipline.setProfessor(professor);
			
			dao.add(discipline);
			
			return "/success"; 
		} catch (Exception e) {
			return "/error"; 
		}
	}
	
	public List<Discipline> listDisciplines(){
		GenericDao<Discipline> dao = new GenericDao<Discipline>(Discipline.class);
		return dao.list();
	}
	
	public List<Discipline> disciplineByStudentName(String studentName) {
		
		GenericDao<Student> dao = new GenericDao<Student>(Student.class);
		List<Student> students = dao.list();
		
		GenericDao<Discipline> daoDisciplines = new GenericDao<Discipline>(Discipline.class);
		List<Discipline> disciplines = daoDisciplines.list();

		
		List<Discipline> dResult = new ArrayList<Discipline>();
		
		for(Student s : students) {
			for(Discipline d : disciplines) {
				if(s.getCourse().getCourseId() == d.getCourse().getCourseId() && s.getName().equals(studentName)) {
					dResult.add(d);
				}
			}
		}
		return dResult;
	}
	
	public List<Discipline> disciplineByCourseId(int courseId) {
		List<Discipline> disciplines = listDisciplines();
		List<Discipline> dResult = new ArrayList<Discipline>();
		for(Discipline d : disciplines) {
			if(d.getCourse().getCourseId() == courseId) {
				dResult.add(d);
			}
		}
		return dResult;	
	}
	
	public int disciplineIdByDisciplineName(String disciplineName) {
		GenericDao<Discipline> dao = new GenericDao<Discipline>(Discipline.class);
		Discipline discipline = dao.findByName(disciplineName);
		return discipline.getDisciplineId();
	}
	
	
}
