package br.com.fiap.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.fiap.entity.User;

@WebFilter(filterName = "LoginFilter", urlPatterns = { "/LoginFilter" })
public class LoginFilter implements Filter {

	public LoginFilter() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		User user = (User) session.getAttribute("user");

		if (user == null) {
			((HttpServletResponse) response).sendRedirect("/javainwebfinalworkclean/login.xhtml");
		}

		if (user != null && !(user.getRole().getRoleId() == 1) && !(user.getRole().getRoleId() == 2)
				&& !(user.getRole().getRoleId() == 3)) {
			((HttpServletResponse) response).sendRedirect("/javainwebfinalworkclean/login.xhtml");
			return;
		}

		String requestPath = ((HttpServletRequest) request).getRequestURI().toLowerCase();

		if (requestPath.contains("/restrict/admin") && user != null && !(user.getRole().getRoleId() == 1)) {
			((HttpServletResponse) response).sendRedirect("/javainwebfinalworkclean/no_access.xhtml");
			return;
		} else if (requestPath.contains("/restrict/professor") && user != null && !(user.getRole().getRoleId() == 3)) {
			((HttpServletResponse) response).sendRedirect("/javainwebfinalworkclean/no_access.xhtml");
			return;
		} else if (requestPath.contains("/restrict/aluno") && user != null && !(user.getRole().getRoleId() == 2)
				&& !(user.getRole().getRoleId() == 1)) {
			((HttpServletResponse) response).sendRedirect("/javainwebfinalworkclean/no_access.xhtml");
			return;
		}

		chain.doFilter(request, response);
	}

	public void destroy() {
	}

}
